package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.r_ele.extractors;

import java.util.List;

import javax.xml.stream.events.XMLEvent;

import hu.unideb.jmdict.jmdict.ReadingElementPriority;

public class Re_RestrExtractor {

	public List<ReadingElementPriority> extractReadingPriorities( List<XMLEvent> readingPrioritiesXmlEvents )	{
		
		//TODO do this
		return null;
	}
	
	public ReadingElementPriority extractReadingPriority( List<XMLEvent> singleReadingPrioritiesXmlEvents, ReadingElementPriority readingElementPriority )	{
		
		//TODO do this
		
		return readingElementPriority;
	}
	
	public ReadingElementPriority extractReadingPriority( List<XMLEvent> singleReadingPrioritiesXmlEvents )	{
		
		ReadingElementPriority readingElementPriority = new ReadingElementPriority();
		
		return extractReadingPriority(singleReadingPrioritiesXmlEvents, readingElementPriority);
	}
	
}
