package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.sense.extractors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import com.neovisionaries.i18n.LanguageAlpha3Code;

import hu.unideb.dictionary.japanese.xml.jmdict.JMDictParser;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.FilterUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.XMLEventUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.GlossHelpers;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JMDictXMLEventsEntry;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.jmdict.jmdict.Gloss;
import hu.unideb.jmdict.jmdict.Sense;

/*
 * 
 * This is going to be used.
 */
public class ExtractUtils {

	public static List<Sense> getListOfSensesForEntry(List<XMLEvent> entry)	{
		
		List<XMLEvent> copiedListOfEntry = new ArrayList<>(entry);
		
		List<XMLEvent> allSensesAsXmlEvents = FilterUtils.filterFor(copiedListOfEntry, JmDictElements.SENSE);
		
		List<Sense> listOfAllSenses = new ArrayList<>();
		
		while( !listOfAllSenses.isEmpty() )	{
			
			List<XMLEvent> xmlEventsSingleSense 
					= XMLEventUtils.filterListOfXmlEventsForSingleElement(allSensesAsXmlEvents, JmDictElements.SENSE);
			
			Sense sense =  ExtractUtils.extractSense(xmlEventsSingleSense);
			
			listOfAllSenses.add(sense);
			
			listOfAllSenses.removeAll(xmlEventsSingleSense);
		}

		return null;
	}
	
	public static Sense extractSense(List<XMLEvent> singleSense)	{
		
		Sense sense = new Sense();
		
		// TODO
		
		return sense;
	}
	
	public static Sense extractSense(List<XMLEvent> singleSense, Sense sense)	{
		
		//TODO
		
		
		return null;
	}
	
	public static List<Gloss> getListOfGlosses(List<XMLEvent> singleSense)	{
		
		List<Gloss> outputListOfGlosses = new ArrayList<>();
		
		List<XMLEvent> copiedListOfSenseXmlEvents = FilterUtils.filterFor(singleSense, JmDictElements.GLOSS);  //new ArrayList<>(singleSense);
		
		//List<XMLEvent> xmlEventsOfASingleSense = 
		
		while( !copiedListOfSenseXmlEvents.isEmpty())	{
			
			List<XMLEvent> xmlEventsOfSingleGloss = FilterUtils.filterForSingleElement( copiedListOfSenseXmlEvents, JmDictElements.GLOSS);
			
			Gloss extractedGlosses = extractGloss(xmlEventsOfSingleGloss);
			
			outputListOfGlosses.add(extractedGlosses);
			
			copiedListOfSenseXmlEvents.removeAll(xmlEventsOfSingleGloss);
			
		}
	
		return outputListOfGlosses;
	}
	
	
	public static Gloss extractGloss( List<XMLEvent> glossSingle, Gloss gloss)	{
		
		//Gloss gloss = new Gloss();
		
		for(XMLEvent event : glossSingle )	{

			if( event.isStartElement() )	{
				
				//Attribute attribute = event.asStartElement().getAttributeByName(new QName("xml", "lang"));
				
				String glossLang = GlossHelpers.extractXmlLangAttributeToGloss(event);
				
				LanguageAlpha3Code lang3 = LanguageAlpha3Code.getByCode(glossLang, false);
				
				gloss.setLanguageCode(lang3);
		
			}
			
			if( event.isCharacters() /* index + 1 < events.size() */ )	{
				
				Characters pcdata = event.asCharacters();

				gloss.setMeaningText( pcdata.getData());
				
			}
		}
		
		return gloss;
	}
	
	public static Gloss extractGloss(List<XMLEvent> glossSingle)	{
		
		Gloss gloss = new Gloss();
		
		return extractGloss(glossSingle, gloss);
	}
	
}
