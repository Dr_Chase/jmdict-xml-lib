package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import hu.unideb.dictionary.japanese.xml.jmdict.Main;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.FilterUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.ExtractFunction;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.GeneralHelpers;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.jmdict.jmdict.Entry;
import hu.unideb.jmdict.jmdict.KanjiElement;
import hu.unideb.jmdict.jmdict.ReadingElement;
import hu.unideb.jmdict.jmdict.Sense;
import hu.unideb.jmdict.repositories.EntryRepository;
import hu.unideb.jmdict.repositories.KanjiElementRepository;
import hu.unideb.jmdict.repositories.ReadingElementRepository;
import hu.unideb.jmdict.repositories.SenseRepository;

@Component
public class EntryExtractors {
	

	private static volatile SenseRepository senseRepository;
	

	private static volatile KanjiElementRepository kanjiElementRepository;
	

	private static volatile ReadingElementRepository readingElementRepository;
	
	private static  volatile EntryRepository entryElementRepository;
	
	private static EntityManager em;
	
	private static AtomicLong count = new AtomicLong();
	
	private static Logger LOG = LoggerFactory
		      .getLogger(EntryExtractors.class);
	
	private static final LocalDateTime startDateTime = LocalDateTime.now();
	
	@Autowired
	public void setSenseRepository(SenseRepository sr)	{
		
		EntryExtractors.senseRepository = sr;
	}
	
	@Autowired
	public void setKanjiElementRepository(KanjiElementRepository kr)	{
		
		EntryExtractors.kanjiElementRepository = kr;
	}
	
	@Autowired
	public void setReadingElementRepository(ReadingElementRepository rer)	{
		
		EntryExtractors.readingElementRepository = rer;
	}
	
	@Autowired
	public void setEntryElementRepository(EntryRepository rer)	{
		
		EntryExtractors.entryElementRepository = rer;
	}
	
	@Autowired
	public void setEntityManager(EntityManager rer)	{
		
		EntryExtractors.em = rer;
	}

	@SuppressWarnings("unchecked")
	public static List<Entry> extractEntries(List<XMLEvent> allEntries)	{
		
		ExtractFunction<Entry> extractFunction = (xmlEventList) -> { return extractEntry(xmlEventList); };
		
		return (List<Entry>) GeneralHelpers.getListOfExtractables(allEntries, JmDictElements.ENTRY, extractFunction);
	}
	
	public static Entry extractEntry(List<XMLEvent> singleEntry, Entry entry) 	{
		
		List<XMLEvent> singleEntrySequenceEvents = FilterUtils.filterForSingleElement(singleEntry, JmDictElements.ENTRY_SQUENCE);
		
		for(XMLEvent event :  singleEntrySequenceEvents)	{
			
			if(event.isCharacters())	{
				
				String entrySeq = event.asCharacters().getData();
				entry.setEntrySequence(Long.parseLong(entrySeq));
			}
		}
		
		//KanjiElements
		/*CompletableFuture<Void> kanjiElementFuture = 
				CompletableFuture.runAsync( () -> {  extractAndSaveKanjiElements(singleEntry, entry); } ); 
		
		//ReadingElements
		CompletableFuture<Void> readingElementsFuture = 
				CompletableFuture.runAsync( () -> { extractAndSaveReadingElements(singleEntry, entry); } ); 
		
		//Sense
		CompletableFuture<Void> senseFuture = 
				CompletableFuture.runAsync( () -> {extractAndSaveSenses(singleEntry, entry);} ); 
		
		CompletableFuture<Void> allFutures = CompletableFuture.allOf(kanjiElementFuture, readingElementsFuture, senseFuture); 
		
		
		try {
			allFutures.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Error when doing async in Entry. ", e);
		} */
		
		extractAndSaveKanjiElements(singleEntry, entry);
		extractAndSaveReadingElements(singleEntry, entry);
		extractAndSaveSenses(singleEntry, entry);
		
		entryElementRepository.saveAndFlush(entry);

		countNumOfEntriesAndLogNthEntry();
		
		return entry;
	}
	
	private static void countNumOfEntriesAndLogNthEntry()	{
		
		Long methodLocalCounterOfWrittenEntries = count.incrementAndGet();
		
		LocalDateTime actualDateTime = LocalDateTime.now();
		
		Duration duration = Duration.between(startDateTime, actualDateTime);
		
		if( (methodLocalCounterOfWrittenEntries % 500L) == 0L )	{
			
			LOG.info("Time spent: " + DurationFormatUtils.formatDuration(duration.toMillis(), "**HH:mm:ss**", true) );
			LOG.info("Number of entries written: " + methodLocalCounterOfWrittenEntries.toString() );
			LOG.info("Number of total entries: " + "182792" );
			long percentage = Math.round ((methodLocalCounterOfWrittenEntries /182792L) * 100L);
			LOG.info("Percentage done: " + percentage + "%" );
		}
		
		
	}
	
	public static void extractAndSaveKanjiElements(List<XMLEvent> singleEntry, Entry entry)	{
		
		List<XMLEvent> kanjiElementXMLEvents = FilterUtils.filterFor(singleEntry, JmDictElements.KANJI_ELEMENT);
		List<KanjiElement> kanjiElements = K_EleExtractors.extractKanjiElements(kanjiElementXMLEvents);
		entry.setChildren(kanjiElements);
		kanjiElementRepository.saveAll(kanjiElements);
		kanjiElementRepository.flush();
	}
	
	public static void extractAndSaveReadingElements(List<XMLEvent> singleEntry, Entry entry)	{
		
		List<XMLEvent> readingElementXMLEvents = FilterUtils.filterFor(singleEntry, JmDictElements.READING_ELEMENT);
		List<ReadingElement> readingElements = R_EleExtractors.extractReadingElements(readingElementXMLEvents);
		entry.setChildren(readingElements);
		readingElementRepository.saveAll(readingElements);
		readingElementRepository.flush();
	}
	
	public static void extractAndSaveSenses(List<XMLEvent> singleEntry, Entry entry)	{
		List<XMLEvent> senseElementXMLEvents = FilterUtils.filterFor(singleEntry, JmDictElements.SENSE );
		List<Sense> senseElements = SenseExtractors.extractSensesForEntry(senseElementXMLEvents);
		entry.setChildren(senseElements);
		senseRepository.saveAll(senseElements);
		senseRepository.flush();
	}
	
	public static Entry extractEntry(List<XMLEvent> singleEntry)	{
		
		Entry entry = new Entry( );
		
		entryElementRepository.save(entry);
		
		return extractEntry(singleEntry, entry);
	}
}
