package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.xml.stream.events.XMLEvent;

import org.magicwerk.brownies.collections.BigList;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.FilterUtils;
import hu.unideb.jmdict.jmdict.interfaces.Extractable;

public class GeneralHelpers {

	@SuppressWarnings("unchecked")
	public static List<? extends Extractable> getListOfExtractables(List<XMLEvent> singleParentElement,
			String subElementName, ExtractFunction<? extends Extractable> extractFunction) {

		List<Extractable> outputListOfElements = new BigList<Extractable>();

		List<XMLEvent> copiedListOfSubElementXmlEvents = FilterUtils.filterFor(singleParentElement, subElementName); // new
																														// ArrayList<>(singleSense);

		while (!copiedListOfSubElementXmlEvents.isEmpty()) {

			List<XMLEvent> xmlEventsOfSingleElement = FilterUtils
					.filterForSingleElement(copiedListOfSubElementXmlEvents, subElementName);

			Extractable extractedExtractable = extractFunction.extract(xmlEventsOfSingleElement);

			outputListOfElements.add(extractedExtractable);

			// copiedListOfSubElementXmlEvents.removeAll(xmlEventsOfSingleElement);
			copiedListOfSubElementXmlEvents = (List<XMLEvent>) removeAllByReferenceInReturnedList(copiedListOfSubElementXmlEvents, xmlEventsOfSingleElement);

		}

		return outputListOfElements; // outputListOfGlosses;
	}

	/*
	 * Motivations: 1. Only SJSXP works with removeAll() so SJSXP has a good equals
	 * and hashcode implementation on XMLEvents 2. We need matching by reference,
	 * because we want to extract a certain sub set of said collection (list). 3.
	 * It's going to be faster and we really need better speed.
	 */
	public static void removeAllByReference(List<? extends Object> listToRemoveFrom,
			List<? extends Object> listOfItemsToRemove) {

		for (int outerIndex = 0; outerIndex < listToRemoveFrom.size(); outerIndex++) {

			for (int innerIndex = 0; innerIndex < listOfItemsToRemove.size(); innerIndex++) {

				if (listToRemoveFrom.get(outerIndex) == listOfItemsToRemove.get(innerIndex)) {

					listToRemoveFrom.set(outerIndex, null);
					break;
				}
			}
		}

		/*
		 * listToRemoveFrom = listToRemoveFrom.parallelStream()
		 * .filter(Objects::nonNull) .collect(Collectors.toList());
		 */

		for (int index = listToRemoveFrom.size() - 1; index >= 0; index--) {

			if (listToRemoveFrom.get(index) == null) {

				listToRemoveFrom.remove(index);
			}
		}
	}

	public static List<? extends Object> removeAllByReferenceInReturnedList(List<? extends Object> listToRemoveFrom,
			List<? extends Object> listOfItemsToRemove) {

		for (int outerIndex = 0; outerIndex < listToRemoveFrom.size(); outerIndex++) {

			for (int innerIndex = 0; innerIndex < listOfItemsToRemove.size(); innerIndex++) {

				if (listToRemoveFrom.get(outerIndex) == listOfItemsToRemove.get(innerIndex)) {

					listToRemoveFrom.set(outerIndex, null);
					break;
				}
			}
		}

		List<Object> outputList = new BigList<>();
		
		for(Object ob : listToRemoveFrom )	{
			
			if( ob != null)	{
				
				outputList.add(ob);
			}
		}
		
		return outputList;
		 

	}
}
