package hu.unideb.dictionary.japanese.xml.jmdict.model.xml;

public class JmDictElements {

	public static String ENTRY = "entry";
	public static String ENTRY_SQUENCE = "ent_seq";
	
	public static String KANJI_ELEMENT = "k_ele";
	
	public static String KANJI_ELEMENT_B = "keb";
	public static String KANJI_ELEMENT_INFO = "ke_inf";
	public static String KANJI_ELEMENT_PRIORITY = "ke_pri";
	
	public static String READING_ELEMENT = "r_ele";
	
	public static String READING_ELEMENT_B = "reb";
	public static String READING_ELEMENT_NOKANJI = "re_nokanji";
	public static String READING_ELEMENT_RESTRICTION = "re_restr";
	public static String READING_ELEMENT_INFO = "re_inf";
	public static String READING_ELEMENT_PRIORITY = "re_pri";
	
	public static String SENSE = "sense";
	
	public static String STAGK = "stagk";
	public static String STAGR = "stagr";
	public static String POS = "pos";
	public static String XREF = "xref";
	public static String ANT = "ant";
	public static String FIELD = "field";
	public static String MISC = "misc";
	public static String S_INF = "s_inf";
	public static String LSOURCE = "lsource";
	public static String DIAL = "dial";
	public static String GLOSS = "gloss";
	
	public static String PRI	 = "pri";
	
}
