package hu.unideb.dictionary.japanese.xml.jmdict;

import java.io.InputStream;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import hu.unideb.dictionary.japanese.xml.jmdict.utils.ResourceHelpers;

public class JMDictParser {
	
	InputStream jmDictIS;
	
	XMLEventReader xmlEventReader;
	
	XMLInputFactory xmlInputFactory;
	
	
	public JMDictParser()	{
		jmDictIS = ResourceHelpers.getResourceAsInputStream ("JMDict");

		xmlInputFactory = XMLInputFactory.newInstance();
		
		try {
			
			xmlEventReader = xmlInputFactory.createXMLEventReader( jmDictIS, "UTF-8");
			
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public JMDictParser(String fileName)	{
		jmDictIS = ResourceHelpers.getResourceAsInputStream (fileName);

		xmlInputFactory = XMLInputFactory.newInstance();
		
		try {
			
			xmlEventReader = xmlInputFactory.createXMLEventReader( jmDictIS, "UTF-8");
			
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	
	public static final String getEventTypeString(int eventType) {
	    switch (eventType) {
	        case XMLEvent.START_ELEMENT:
	            return "START_ELEMENT";

	        case XMLEvent.END_ELEMENT:
	            return "END_ELEMENT";

	        case XMLEvent.PROCESSING_INSTRUCTION:
	            return "PROCESSING_INSTRUCTION";

	        case XMLEvent.CHARACTERS:
	            return "CHARACTERS";

	        case XMLEvent.COMMENT:
	            return "COMMENT";

	        case XMLEvent.START_DOCUMENT:
	            return "START_DOCUMENT";

	        case XMLEvent.END_DOCUMENT:
	            return "END_DOCUMENT";

	        case XMLEvent.ENTITY_REFERENCE:
	            return "ENTITY_REFERENCE";

	        case XMLEvent.ATTRIBUTE:
	            return "ATTRIBUTE";

	        case XMLEvent.DTD:
	            return "DTD";

	        case XMLEvent.CDATA:
	            return "CDATA";

	        case XMLEvent.SPACE:
	            return "SPACE";
	            
	        default:
	        	return "UNKNOWN_EVENT_TYPE , " + eventType;
	    }
	    
	}
}
