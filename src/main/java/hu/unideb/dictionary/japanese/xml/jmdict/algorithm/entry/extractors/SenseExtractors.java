package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.events.XMLEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.FilterUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.XMLEventUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.ExtractFunction;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.GeneralHelpers;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.sense.extractors.ExtractUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.sense.extractors.GlossExtractor;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.jmdict.jmdict.Gloss;
import hu.unideb.jmdict.jmdict.Sense;
import hu.unideb.jmdict.repositories.EntryRepository;
import hu.unideb.jmdict.repositories.GlossRepository;
import hu.unideb.jmdict.repositories.KanjiElementRepository;
import hu.unideb.jmdict.repositories.ReadingElementRepository;
import hu.unideb.jmdict.repositories.SenseRepository;

@Component
public class SenseExtractors {
	

	private static volatile SenseRepository senseRepository;

	private static volatile KanjiElementRepository kanjiElementRepository;

	private static volatile ReadingElementRepository readingElementRepository;
	
	private static volatile EntryRepository entryElementRepository;
	
	private static volatile GlossRepository glossElementRepository;
	
	@Autowired
	public void setSenseRepository(SenseRepository sr)	{
		
		senseRepository = sr;
	}
	
	@Autowired
	public void setKanjiElementRepository(KanjiElementRepository kr)	{
		
		kanjiElementRepository = kr;
	}
	
	@Autowired
	public void setReadingElementRepository(ReadingElementRepository rer)	{
		
		readingElementRepository = rer;
	}
	
	@Autowired
	public void setEntryElementRepository(EntryRepository rer)	{
		
		entryElementRepository = rer;
	}
	
	@Autowired
	public void setGlossRepository(GlossRepository sr)	{
		
		glossElementRepository = sr;
	}
	

	@SuppressWarnings("unchecked")
	public static List<Sense> extractSensesForEntry(List<XMLEvent> singleEntry) {

		ExtractFunction<Sense> ef = (eventList) -> {
			return extractSense(eventList);
		};

		return (List<Sense>) GeneralHelpers.getListOfExtractables(singleEntry, JmDictElements.SENSE, ef);
		/*
		 * //old code List<XMLEvent> copiedListOfEntry = new ArrayList<>(singleEntry);
		 * 
		 * List<XMLEvent> allSensesAsXmlEvents =
		 * FilterUtils.filterFor(copiedListOfEntry, JmDictElements.SENSE);
		 * 
		 * List<Sense> listOfAllSenses = new ArrayList<>();
		 * 
		 * while( !allSensesAsXmlEvents.isEmpty() ) {
		 * 
		 * List<XMLEvent> xmlEventsSingleSense =
		 * XMLEventUtils.filterListOfXmlEventsForSingleElement(allSensesAsXmlEvents,
		 * JmDictElements.SENSE);
		 * 
		 * Sense sense = extractSense(xmlEventsSingleSense);
		 * 
		 * listOfAllSenses.add(sense);
		 * 
		 * allSensesAsXmlEvents.removeAll(xmlEventsSingleSense); }
		 * 
		 * return listOfAllSenses;
		 * 
		 */
	}

	public static Sense extractSense(List<XMLEvent> singleSense) {

		Sense sense = new Sense();
		
		senseRepository.save(sense);

		return extractSense(singleSense, sense);
	}

	public static Sense extractSense(List<XMLEvent> singleSense, Sense sense) {

		// TODO stagk

		// TODO stagr

		// TODO pos

		// TODO xref

		// TODO ant

		// TODO field

		// TODO misc

		// TODO s_inf

		// TODO lsource

		// TODO dial

		// gloss

		List<Gloss> glosses = GlossExtractor.getListOfGlosses(singleSense);

		sense.setChildren(glosses);
		
		/*
		for(Gloss gloss : glosses)	{
			
			glossElementRepository.save(gloss);
		} */
		
		glossElementRepository.saveAll(glosses);

		return sense;
	}
}
