package hu.unideb.dictionary.japanese.xml.jmdict.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang3.StringUtils;

import com.neovisionaries.i18n.LanguageCode;

import hu.unideb.dictionary.japanese.xml.jmdict.model.interfaces.JMDictSearch;
import hu.unideb.dictionary.japanese.xml.jmdict.model.interfaces.SearchData;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JMDictXMLEventsEntry;
import hu.unideb.dictionary.japanese.xml.jmdict.utils.XMLDocumentHandler;

public class SimpleJMDictSearch implements JMDictSearch  {

	
	
	/*
	 * Non Javadox
	 * ez itt eltárolja az összes eventsorozatot ami egy JMDict entry, de olyan JMDict entry, aminek a valamelyik gloss
	 * elementje tartalmazza a kereset szöveget.
	 */
	protected static List<JMDictXMLEventsEntry> searchForJapSource(SearchData searchData) throws XMLStreamException	{
		
		List<JMDictXMLEventsEntry> jmDictEntries = new ArrayList<JMDictXMLEventsEntry>();
		
		XMLEventReader xmlEventReader = XMLDocumentHandler.getEventReaderForJMDict();
		
		List<XMLEvent> jmDictEntryEvents = new ArrayList<XMLEvent>();

		boolean areWeAtTheStartOfEntry = false;
		boolean areWeAtTheEndOfEntry = false;
		
		boolean matchingWordFound = false;

		while (xmlEventReader.hasNext()) {

			XMLEvent event = xmlEventReader.nextEvent();

			if (event.isStartElement()) {

				if (event.asStartElement().getName().toString().equals("entry")) {
					
					jmDictEntryEvents = new ArrayList<XMLEvent>();
					areWeAtTheStartOfEntry = true;
				}
			}

			if (areWeAtTheStartOfEntry == true && areWeAtTheEndOfEntry == false) {

				jmDictEntryEvents.add(event);
			}

			if (event.isEndElement()) {

				if (event.asEndElement().getName().toString().equals("entry")) {

					areWeAtTheEndOfEntry = true;
				}
			}
			
			if( event.isEndElement() && jmDictEntryEvents.size() >= 2 )	{
				
				XMLEvent previousEvent = jmDictEntryEvents.get( jmDictEntryEvents.size() - 2 );
				
				if( event.asEndElement().getName().toString().equals("gloss") && 
						previousEvent.isCharacters() )	{
					
					String glossText = previousEvent.asCharacters().getData();
					
					if( StringUtils.contains(glossText, searchData.getWord()) )	{
						matchingWordFound = true;
					}	
				}
			}

			if (areWeAtTheStartOfEntry && areWeAtTheEndOfEntry) {

				if( matchingWordFound )	{
					
					jmDictEntries.add(new JMDictXMLEventsEntry(jmDictEntryEvents));
				}
				areWeAtTheStartOfEntry = false;
				areWeAtTheEndOfEntry = false;
			}
		}
		xmlEventReader.close();
		
		return jmDictEntries;
	}
}
