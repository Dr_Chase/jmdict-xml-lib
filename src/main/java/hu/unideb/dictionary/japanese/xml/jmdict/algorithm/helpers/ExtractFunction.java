package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers;

import java.util.List;

import javax.xml.stream.events.XMLEvent;

import hu.unideb.jmdict.jmdict.interfaces.Extractable;

@FunctionalInterface
public interface ExtractFunction<T extends Extractable> {

	//public abstract Extractable extract(List<XMLEvent> singleExtractableElement, Extractable extractable);
	
	public abstract T extract(List<XMLEvent> singleExtractableElement);
}
