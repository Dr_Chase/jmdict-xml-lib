package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors;

import java.util.List;

import javax.xml.stream.events.XMLEvent;

import org.springframework.beans.factory.annotation.Autowired;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.FilterUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.ExtractFunction;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.GeneralHelpers;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.jmdict.jmdict.KanjiElement;
import hu.unideb.jmdict.repositories.EntryRepository;
import hu.unideb.jmdict.repositories.KanjiElementRepository;
import hu.unideb.jmdict.repositories.ReadingElementRepository;
import hu.unideb.jmdict.repositories.SenseRepository;

public class K_EleExtractors {

	private static SenseRepository senseRepository;
	

	private static KanjiElementRepository kanjiElementRepository;
	

	private static ReadingElementRepository readingElementRepository;
	
	private static EntryRepository entryElementRepository;
	
	@Autowired
	public void setSenseRepository(SenseRepository sr)	{
		
		K_EleExtractors.senseRepository = sr;
	}
	
	@Autowired
	public void setKanjiElementRepository(KanjiElementRepository kr)	{
		
		K_EleExtractors.kanjiElementRepository = kr;
	}
	
	@Autowired
	public void setReadingElementRepository(ReadingElementRepository rer)	{
		
		K_EleExtractors.readingElementRepository = rer;
	}
	
	@Autowired
	public void setEntryElementRepository(EntryRepository rer)	{
		
		K_EleExtractors.entryElementRepository = rer;
	}
	
	@SuppressWarnings("unchecked")
	public static List<KanjiElement> extractKanjiElements( List<XMLEvent> allKanjiElementXmlEvents)	{
		
		ExtractFunction<KanjiElement> extractFunction = (xmlEventList) -> { return extractKanjiElement(xmlEventList); };
		
		return (List<KanjiElement>) GeneralHelpers.getListOfExtractables(allKanjiElementXmlEvents, JmDictElements.KANJI_ELEMENT, extractFunction);
	}
	
	public static KanjiElement extractKanjiElement( List<XMLEvent> singleKanjiElementXmlEvents, KanjiElement kanjiElement )	{
		
		// dtd allows a single keb element inside k_ele 
		List<XMLEvent> xmlEventsOfKEBs 
				= FilterUtils.filterForSingleElement(singleKanjiElementXmlEvents, JmDictElements.KANJI_ELEMENT_B);
		
		for( XMLEvent event : xmlEventsOfKEBs )	{
			
			if( event.isCharacters() )	{
				
				String pcData = event.asCharacters().getData();
				kanjiElement.setKanjiElement(pcData);
			}
		}
		
		// TODO ke_inf
		
		// TODO ke_pri
		
		return kanjiElement;
	}
	
	public static KanjiElement extractKanjiElement( List<XMLEvent> singleKanjiElementXmlEvents)	{
		
		KanjiElement kanjiElement = new KanjiElement();
		
		
		
		return extractKanjiElement(singleKanjiElementXmlEvents, kanjiElement);
	}
}
