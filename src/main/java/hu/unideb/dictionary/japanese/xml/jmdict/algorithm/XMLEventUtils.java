package hu.unideb.dictionary.japanese.xml.jmdict.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang3.StringUtils;
import org.magicwerk.brownies.collections.BigList;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.GeneralHelpers;
import hu.unideb.dictionary.japanese.xml.jmdict.model.interfaces.SearchData;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JMDictXMLEventsEntry;
import hu.unideb.dictionary.japanese.xml.jmdict.utils.XMLDocumentHandler;
import hu.unideb.jmdict.jmdict.Entry;
import hu.unideb.jmdict.jmdict.KanjiElement;
import hu.unideb.jmdict.jmdict.KanjiElementInfo;

public class XMLEventUtils {

	public static Entry createEntryFromXmlEvents(JMDictXMLEventsEntry jMDictXMLEventsEntry) {

		List<XMLEvent> xmlEventsFullEntry = jMDictXMLEventsEntry.getEntry();

		List<XMLEvent> xmlEventsFiltered;

		return null;
	}

	/**
	 * Creates a KanjiElement and has a side effect, removes all elements from input
	 * List which was used to create the KanjiElement.
	 * 
	 * @param xmlEventsFilteredContainingKanjiElements
	 * @return
	 */
	public static KanjiElement createKanjiElement(List<XMLEvent> xmlEventsFilteredContainingKanjiElements) {

		return null;
	}

	public static KanjiElementInfo createKanjiElementInformation() {

		return null;
	}

	public static List<XMLEvent> filterListOfXmlEventsForSingleElement(List<XMLEvent> xmlEvents, String elementName) {

		return genericXMLEventFilter(xmlEvents, elementName, true, (filteredList, event) -> {
			filteredList.add(event);
		});

	}

	public static List<XMLEvent> filterListOfXmlEvents(List<XMLEvent> xmlEvents, String elementName) {

		return genericXMLEventFilter(xmlEvents, elementName, false, (filteredList, event) -> {
			filteredList.add(event);
		});
	}

	/**
	 * A generic method for operating with XML Events and filtering them. The
	 * FilteringFunction works in a scope where the XMLEvent is surely between (and
	 * including) an opening XML event with name elementName, and an enclosing
	 * element with name elementName. The method always returns a new list, separate
	 * from the input argument xmlEvents.
	 * 
	 * @param xmlEvents
	 * @param elementName the string of the xml tag
	 * @return
	 */
	public static List<XMLEvent> genericXMLEventFilter(List<XMLEvent> xmlEvents, String elementName,
			boolean isSingleElement, FilteringFunction ff) {

		List<XMLEvent> filteredList = new BigList<>();

		boolean areWeAtTheStartOfEntry = false;
		boolean areWeAtTheEndOfEntry = false;

		for (XMLEvent event : xmlEvents) {

			if (event.isStartElement()) {

				if (event.asStartElement().getName().toString().equals(elementName)) {

					areWeAtTheStartOfEntry = true;
				}
			}

			if (areWeAtTheStartOfEntry == true && areWeAtTheEndOfEntry == false) {

				ff.filter(filteredList, event);
			}

			if (event.isEndElement()) {

				if (event.asEndElement().getName().toString().equals(elementName)) {

					areWeAtTheEndOfEntry = true;
				}
			}

			if (areWeAtTheStartOfEntry && areWeAtTheEndOfEntry) {

				if (isSingleElement) {
					break;
				}
				areWeAtTheStartOfEntry = false;
				areWeAtTheEndOfEntry = false;
			}
		}

		return filteredList;
	}

	public static List<XMLEvent> genericXMLEventFilter(List<XMLEvent> xmlEvents, String elementName,
			boolean isSingleElement, FilteringFunction ff, int start) {

		List<XMLEvent> filteredList = new BigList<>();

		boolean areWeAtTheStartOfEntry = false;
		boolean areWeAtTheEndOfEntry = false;

		for (int index = start; index < xmlEvents.size() ; index++ ) {
			XMLEvent event = xmlEvents.get( index );

			if (event.isStartElement()) {

				if (event.asStartElement().getName().toString().equals(elementName)) {

					areWeAtTheStartOfEntry = true;
				}
			}

			if (areWeAtTheStartOfEntry == true && areWeAtTheEndOfEntry == false) {

				ff.filter(filteredList, event);
			}

			if (event.isEndElement()) {

				if (event.asEndElement().getName().toString().equals(elementName)) {

					areWeAtTheEndOfEntry = true;
				}
			}

			if (areWeAtTheStartOfEntry && areWeAtTheEndOfEntry) {

				if (isSingleElement) {
					break;
				}
				areWeAtTheStartOfEntry = false;
				areWeAtTheEndOfEntry = false;
			}
		}

		return filteredList;
	}

}