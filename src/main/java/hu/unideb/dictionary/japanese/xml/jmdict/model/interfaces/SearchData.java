package hu.unideb.dictionary.japanese.xml.jmdict.model.interfaces;

import com.neovisionaries.i18n.LanguageCode;

public interface SearchData {

	public LanguageCode getSourceLang();
	public LanguageCode getTargetLang();
	
	public String getWord();
	
	public Object getSearchData();
	public Class<?> getSearchDataType();
}
