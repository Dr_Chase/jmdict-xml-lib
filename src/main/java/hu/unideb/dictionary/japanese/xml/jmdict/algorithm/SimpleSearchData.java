package hu.unideb.dictionary.japanese.xml.jmdict.algorithm;

import com.neovisionaries.i18n.LanguageCode;

import hu.unideb.dictionary.japanese.xml.jmdict.model.interfaces.SearchData;

public class SimpleSearchData implements SearchData {

	
	private final String word;
	private final LanguageCode sourceLang;
	private final LanguageCode targetLang;
	
	public SimpleSearchData(String word, LanguageCode sourceLang, LanguageCode targetLang) {
		super();
		this.word = word;
		this.sourceLang = sourceLang;
		this.targetLang = targetLang;
	}

	public String getWord() {
		return word;
	}

	public LanguageCode getSourceLang() {
		return sourceLang;
	}

	public LanguageCode getTargetLang() {
		return targetLang;
	}

	public Object getSearchData() {
		// TODO Auto-generated method stub
		return null;
	}

	public Class<?> getSearchDataType() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
