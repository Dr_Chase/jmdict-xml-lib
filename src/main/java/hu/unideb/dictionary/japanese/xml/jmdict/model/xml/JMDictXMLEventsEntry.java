package hu.unideb.dictionary.japanese.xml.jmdict.model.xml;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.xml.stream.events.XMLEvent;

public class JMDictXMLEventsEntry {

	private final List<XMLEvent> entry;

	public JMDictXMLEventsEntry(List<XMLEvent> input) {
		super();
		entry = new CopyOnWriteArrayList<XMLEvent>(input);
	}

	public JMDictXMLEventsEntry() {
		super();
		entry = new CopyOnWriteArrayList<XMLEvent>();
	}

	public List<XMLEvent> getEntry() {
		return entry;
	}
	
	/*
	@Override
	public String toString()	{
		
		for( XMLEvent event :  entry)	{
			
		}
	}
	*/
	
}
