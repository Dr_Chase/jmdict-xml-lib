package hu.unideb.dictionary.japanese.xml.jmdict.algorithm;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.magicwerk.brownies.collections.BigList;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.GeneralHelpers;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JMDictXMLEventsEntry;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;

public class FilterUtils {

	public static List<XMLEvent> filterEntryForSenses(List<XMLEvent> entry)	{
		
		return filterFor(entry, JmDictElements.SENSE);
	}
	
	/**
	 * Thing.
	 * @param xmlEvents
	 * @param elementName
	 * @param numberOfEntries, long startNumOfXMLEvents
	 * @return
	 */
	public static Pair< List<XMLEvent>, Integer> filterListOfXmlEventsForXNumOfElements(List<XMLEvent> xmlEvents, String elementName, int numberOfEntries, int startNumOfXMLEvents) {

		List<XMLEvent> output = new BigList<>();
		
		int numOfPassedXMLEvents = startNumOfXMLEvents;
		for(int index = 0 ; index < numberOfEntries ; index++)	{
			

			List<XMLEvent> forList = filterForSingleElementFromIndex(xmlEvents, elementName, numOfPassedXMLEvents);
			 output.addAll(forList);
			 
			 numOfPassedXMLEvents = numOfPassedXMLEvents + forList.size();

			 
		}
		Pair< List<XMLEvent>, Integer> outputPair = new MutablePair<>(output, Integer.valueOf( numOfPassedXMLEvents) );
		return outputPair;
	}
	
	public static List<XMLEvent> filterForSingleElementFromIndex(List<XMLEvent> entry, String retainableElementName, int startIndex)	{
		
		boolean singleElementPlease = true;
		return XMLEventUtils.genericXMLEventFilter(entry, retainableElementName, singleElementPlease, (filteredList, event) -> { filteredList.add(event); }, startIndex );
	}
	
	public static List<XMLEvent> filterForSingleElement(List<XMLEvent> entry, String retainableElementName)	{
		
		boolean singleElementPlease = true;
		return XMLEventUtils.genericXMLEventFilter(entry, retainableElementName, singleElementPlease, (filteredList, event) -> { filteredList.add(event); } );
	}
	
	public static List<XMLEvent> filterFor(List<XMLEvent> entry, String retainableElementName)	{
		

		boolean allElementsPleaseNotOnlyASingleOne = false;
		return XMLEventUtils.genericXMLEventFilter(entry, retainableElementName, allElementsPleaseNotOnlyASingleOne, (filteredList, event) -> { filteredList.add(event); } );
	}
}
