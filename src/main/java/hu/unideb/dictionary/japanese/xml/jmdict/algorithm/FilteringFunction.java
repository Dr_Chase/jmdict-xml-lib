package hu.unideb.dictionary.japanese.xml.jmdict.algorithm;

import java.util.List;

import javax.xml.stream.events.XMLEvent;

@FunctionalInterface
interface FilteringFunction {

	public abstract void filter(List<XMLEvent> xmlEvents, XMLEvent actualXMLEvent);
}
