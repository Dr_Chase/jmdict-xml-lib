package hu.unideb.dictionary.japanese.xml.jmdict;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.magicwerk.brownies.collections.BigList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.FilterUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors.EntryExtractors;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.dictionary.japanese.xml.jmdict.utils.XMLDocumentHandler;
import hu.unideb.jmdict.repositories.EntryRepository;
import hu.unideb.jmdict.jmdict.Entry;

@SpringBootApplication
@EnableJpaRepositories(basePackages="hu.unideb.jmdict.repositories")
@ComponentScan(basePackages = {"hu.unideb.jmdict.jmdict", 
								"hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors"})
@EntityScan("hu.unideb.jmdict.jmdict")
public class Main implements CommandLineRunner {
	
	private static final int batchnumber = 46000;
	
	@Autowired
	EntryRepository entryRepository;
 
    private static Logger LOG = LoggerFactory
      .getLogger(Main.class);
 
    public static void main(String[] args) {
        LOG.info("STARTING THE APPLICATION");
        SpringApplication.run(Main.class, args);
        LOG.info("APPLICATION FINISHED");
    }
  
    @Override
    public void run(String... args) throws XMLStreamException {
        LOG.info("EXECUTING : command line runner");
        
        LocalTime startTime = LocalTime.now();
        LOG.info("Starting time: " + startTime.format(DateTimeFormatter.ISO_LOCAL_TIME));
  
        for (int i = 0; i < args.length; ++i) {
            LOG.info("args[{}]: {}", i, args[i]);
        }
        
        //XMLEventReader eventReader = XMLDocumentHandler.getEventReaderFor("JMDict-sample1.xml");
        XMLEventReader eventReader = XMLDocumentHandler.getEventReaderFor("JMdict");
        List<XMLEvent> allXMLEvents = new BigList<>();
        
        while(eventReader.hasNext())	{
        	
        	XMLEvent event = eventReader.nextEvent();
        	allXMLEvents.add(event);
        }
        
        List<XMLEvent> allEntriesAsXMLEvents = FilterUtils.filterFor(allXMLEvents, JmDictElements.ENTRY);
        
        
        Pair< List<XMLEvent>, Integer> firstPair =
        		FilterUtils.filterListOfXmlEventsForXNumOfElements(allEntriesAsXMLEvents, JmDictElements.ENTRY, batchnumber, Integer.valueOf(0));
        List<XMLEvent> firstBatch = firstPair.getLeft();
        LOG.info("number of firstbatch events: " + firstBatch.size());
        
        Pair< List<XMLEvent>, Integer> secondPair =
        		FilterUtils.filterListOfXmlEventsForXNumOfElements(allEntriesAsXMLEvents, JmDictElements.ENTRY, batchnumber, firstPair.getRight());
        List<XMLEvent> secondBatch = secondPair.getLeft();
        LOG.info("number of secondbatch events: " + secondBatch.size());
        
        Pair< List<XMLEvent>, Integer> thirdPair =
        		FilterUtils.filterListOfXmlEventsForXNumOfElements(allEntriesAsXMLEvents, JmDictElements.ENTRY, batchnumber, secondPair.getRight());
        List<XMLEvent> thirdBatch = thirdPair.getLeft();
        LOG.info("number of thirdbatch events: " + thirdBatch.size());
        
        Pair< List<XMLEvent>, Integer> forthPair =
        		FilterUtils.filterListOfXmlEventsForXNumOfElements(allEntriesAsXMLEvents, JmDictElements.ENTRY, batchnumber, thirdPair.getRight());
        List<XMLEvent> fourthBatch = forthPair.getLeft();
        LOG.info("number of forthbatch events: " + fourthBatch.size());
        
        LOG.info("number of allEntriesAsXMLEvents events: " + allEntriesAsXMLEvents.size());
        
        int sumOfBatches = firstBatch.size() + secondBatch.size() + thirdBatch.size() + fourthBatch.size();
        LOG.info("number of sum of batches of XML events: " + Integer.toString(sumOfBatches) );
        
        CompletableFuture<Void> firstFuture = CompletableFuture.runAsync( () -> { EntryExtractors.extractEntries(firstBatch); } );
        
        
        CompletableFuture<Void> secondFuture = CompletableFuture.runAsync( () -> { EntryExtractors.extractEntries(secondBatch); } );
        
        CompletableFuture<Void> thirdFuture = CompletableFuture.runAsync( () -> { EntryExtractors.extractEntries(thirdBatch); } );
        
        CompletableFuture<Void> fourthFuture = CompletableFuture.runAsync( () -> { EntryExtractors.extractEntries(fourthBatch); } );
        
        CompletableFuture<Void> allFutures = CompletableFuture.allOf(firstFuture, secondFuture, thirdFuture, fourthFuture);
        
        

       
        
        try {
			allFutures.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("error during main allfutures get", e);
		}  
        
        //List<Entry> allEntries = EntryExtractors.extractEntries(allEntriesAsXMLEvents);
        
        LocalTime endTime = LocalTime.now();
        LOG.info("Finish time: " + endTime.format(DateTimeFormatter.ISO_LOCAL_TIME));
        Duration timeSpent = Duration.between(startTime, endTime);
        
        LOG.info("Time spent: " + DurationFormatUtils.formatDuration(timeSpent.toMillis(), "**H:mm:ss**", true) );
    }
}