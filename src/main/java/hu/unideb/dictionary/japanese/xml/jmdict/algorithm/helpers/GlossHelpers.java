package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers;

import java.util.Iterator;

import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;

import hu.unideb.jmdict.jmdict.Gloss;

public class GlossHelpers {

	/*
	 * Takes in a event which must be a start element!
	 * Must use Xerces, because Woodstox has a malfunction here
	 * namespaces and localnames are not recognized consistently within xerces so we must iter it 
	 * 
	 * this one is not reusable, it works only with gloss lang attributes
	 * 1 gloss element can have only one xml:lang attribute so this will work:
	 */
	@SuppressWarnings("unchecked")
	public static String extractXmlLangAttributeToGloss(XMLEvent eventWhichIsAStartElementOfAGloss)	{
		
		String output = null;
		
		if(!eventWhichIsAStartElementOfAGloss.isStartElement())	{
			
			throw new IllegalArgumentException("extractAttributesToGloss method needs an event which is a StartElement!");
		}
		
		
		Iterator iter = eventWhichIsAStartElementOfAGloss.asStartElement().getAttributes();
		
		while(iter.hasNext())	{
			
			Object attribute = iter.next();  //Type in case of Xerces is com.sun.xml.internal.stream.events.AttributeImpl
											// Type with woodstox or stax2 is org.codehaus.stax2.ri.evt.AttributeEventImpl
 
			/* com.sun.xml.internal.stream.events.AttributeImpl attributeCasted 			//For Xerces default sun java impl
					= (com.sun.xml.internal.stream.events.AttributeImpl)  attribute; */
			
			//org.codehaus.stax2.ri.evt.AttributeEventImpl attributeCasted 			
			//= ( org.codehaus.stax2.ri.evt.AttributeEventImpl)  attribute;

			
			output = attribute.toString();
			
			break;
		}
		
		return GlossHelpers.extractThreeLetterLangCodeAsString(output);
	}
	
	/*
	 * will extract the 3 letter lang code from the xml:lang attribute strings,
	 * input must be something like \"xml:lang='eng'\" or xml:lang='ger' etc....
	 * see: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3
	 */
	public static String extractThreeLetterLangCodeAsString(String inputXmlLang)	{
		
		String output =  RegExUtils.removeFirst(inputXmlLang, "xml:lang='");
		output = StringUtils.substringBefore(output, "'");
		
		if( output.length() != 3 )	{
			
			output =  RegExUtils.removeFirst(inputXmlLang, "xml:lang=\"");
			output = StringUtils.substringBefore(output, "\"");
		}
		
		return output;
	}
}
