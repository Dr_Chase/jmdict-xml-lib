package hu.unideb.dictionary.japanese.xml.jmdict.algorithm;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang3.StringUtils;

//import com.ctc.wstx.stax.WstxInputFactory;

import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JMDictXMLEventsEntry;
import hu.unideb.dictionary.japanese.xml.jmdict.utils.ResourceHelpers;

public class XMLEventParser {

	/*
	 * This will make every entry from the file a JMDictXMLEventsEntry, which can be used later.
	 */
	public static List<JMDictXMLEventsEntry>  getJMDictXMLEventsEntryFromFile() throws XMLStreamException {

		InputStream jmDictIS;

		XMLEventReader xmlEventReader;

		XMLInputFactory xmlInputFactory;

		jmDictIS = ResourceHelpers.getResourceAsInputStream("JMDict");

		// So far I have not been able to change entity Expansion Limit (100000) which
		// caused problem, so we turned it off
		// https://docs.oracle.com/javase/tutorial/jaxp/limits/using.html
		// System.setProperty("jdk.xml.entityExpansionLimit", "200000");

		// https://docs.oracle.com/javase/tutorial/jaxp/limits/limits.html <--- this man
		// is the best
		// System.setProperty("jdk.xml.maxGeneralEntitySizeLimit", "2000");

		xmlInputFactory = XMLInputFactory.newInstance();

		//((WstxInputFactory) xmlInputFactory).configureForLowMemUsage();

		xmlInputFactory.setProperty("javax.xml.stream.isReplacingEntityReferences", new Boolean(false));

		// assertTrue(xmlInputFactory.isPropertySupported("ENTITY_EXPANSION_LIMIT"));
		// xmlInputFactory.setProperty("ENTITY_EXPANSION_LIMIT", new Integer(200000) );
		// xmlInputFactory.setProperty(name, value);

		List<JMDictXMLEventsEntry> jmDictEntries = new ArrayList<JMDictXMLEventsEntry>();

		xmlEventReader = xmlInputFactory.createXMLEventReader(jmDictIS, "UTF-8");

		List<XMLEvent> jmDictEntryEvents = new ArrayList<XMLEvent>();

		boolean areWeAtTheStartOfEntry = false;
		boolean areWeAtTheEndOfEntry = false;

		while (xmlEventReader.hasNext() ) {

			XMLEvent event = xmlEventReader.nextEvent();

			if (event.isStartElement()) {

				if (event.asStartElement().getName().toString().equals("entry")) {

					jmDictEntryEvents = new ArrayList<XMLEvent>();
					areWeAtTheStartOfEntry = true;
				}

			}

			if (areWeAtTheStartOfEntry == true && areWeAtTheEndOfEntry == false) {

				jmDictEntryEvents.add(event);
			}

			if (event.isEndElement()) {

				if (event.asEndElement().getName().toString().equals("entry")) {

					areWeAtTheEndOfEntry = true;
				}

			}

			if (areWeAtTheStartOfEntry && areWeAtTheEndOfEntry) {

				jmDictEntries.add(new JMDictXMLEventsEntry(jmDictEntryEvents));


				areWeAtTheStartOfEntry = false;
				areWeAtTheEndOfEntry = false;

			}
		}

		xmlEventReader.close();
		return jmDictEntries;

	}
}
