package hu.unideb.dictionary.japanese.xml.jmdict.utils;

import java.io.File;
import java.io.InputStream;

public class ResourceHelpers {

	public static InputStream getResourceAsInputStream(String fileNameAndPath)	{
		InputStream inputStream = ResourceHelpers.class
				.getClassLoader().getResourceAsStream(fileNameAndPath);
		return inputStream;
	}
	
	public static File getResourceAsFile(String fileNameAndPath)	{
		File file = new File( ResourceHelpers.class
				.getClassLoader().getResource(fileNameAndPath).getFile() );
		return file;
	}
	
	
}
