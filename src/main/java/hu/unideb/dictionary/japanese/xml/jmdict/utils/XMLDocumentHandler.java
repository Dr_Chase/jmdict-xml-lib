package hu.unideb.dictionary.japanese.xml.jmdict.utils;

import java.io.InputStream;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;

//import org.codehaus.stax2.XMLInputFactory2;



public class XMLDocumentHandler {
	
	//InputStream jmDictIS;

	//XMLEventReader xmlEventReader;

	static XMLInputFactory xmlInputFactory;

	static {
		
		// So far I have not been able to change entity Expansion Limit (100000) which caused problem, so we turned it off
		// https://docs.oracle.com/javase/tutorial/jaxp/limits/using.html
		System.setProperty("jdk.xml.entityExpansionLimit", "2000000");
		
		// https://docs.oracle.com/javase/tutorial/jaxp/limits/limits.html   <--- this man is the best
		//System.setProperty("jdk.xml.maxGeneralEntitySizeLimit", "2000");
		
		
		
		
		xmlInputFactory = XMLInputFactory.newInstance();	//For Xerces default Java
		
		
		//xmlInputFactory = XMLInputFactory2.newInstance();
		//((XMLInputFactory2) xmlInputFactory  ).configureForXmlConformance();	// aalto
		//((XMLInputFactory2) xmlInputFactory  ).	// aalto
		
		//((WstxInputFactory) xmlInputFactory  ) .configureForLowMemUsage();
		
		xmlInputFactory.setProperty("javax.xml.stream.isReplacingEntityReferences", new Boolean(false));
		xmlInputFactory.setProperty("javax.xml.stream.isCoalescing", Boolean.TRUE);
		
	}
	
	/**
	 * Returns an XMLEventReader based on the file name of the resource.
	 * @param documentNameInResources
	 * @return
	 * @throws XMLStreamException
	 */
	public static XMLEventReader getEventReaderFor(String documentNameInResources) throws XMLStreamException	{
		
		InputStream jmDictIS = ResourceHelpers.getResourceAsInputStream(documentNameInResources);
		
		return xmlInputFactory.createXMLEventReader(jmDictIS, "UTF-8");
		
	}

	public static XMLEventReader getEventReaderForJMDict() throws XMLStreamException	{
		return getEventReaderFor("JMDict");
	}
	
	public static XMLEventReader getEventReaderForKanjiDic() throws XMLStreamException	{
		return getEventReaderFor("kanjidic2.xml");
	}
	
	
	
	
}
