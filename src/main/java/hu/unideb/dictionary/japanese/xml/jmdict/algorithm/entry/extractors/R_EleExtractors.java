package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors;

import java.util.List;

import javax.xml.stream.events.XMLEvent;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.FilterUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.ExtractFunction;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.GeneralHelpers;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.jmdict.jmdict.Gloss;
import hu.unideb.jmdict.jmdict.ReadingElement;

public class R_EleExtractors {

	
	@SuppressWarnings("unchecked")
	public static List<ReadingElement> extractReadingElements(List<XMLEvent> singleEntry)	{
		
		ExtractFunction<ReadingElement> extractFunction = (xmlEvents) -> { return extractReadingElement(xmlEvents); };
		
		return (List<ReadingElement>) GeneralHelpers.getListOfExtractables(singleEntry, 
				JmDictElements.READING_ELEMENT, 
				extractFunction );
	}
	
	
	public static ReadingElement extractReadingElement( List<XMLEvent> readingElementSingle, ReadingElement readingElement)	{
		
		// DTD lets a single reb element
		List<XMLEvent> singleRebElement = FilterUtils.filterForSingleElement(readingElementSingle, JmDictElements.READING_ELEMENT_B);
		
		for( XMLEvent event :  singleRebElement)	{
			
			if( event.isCharacters() )	{
				
				readingElement.setReadingElementText( event.asCharacters().getData() );
			}
		}
		
		// TODO re_nokanji
		
		//TODO re_restr
		
		//TODO re_inf
		
		//TODO re_pri
		
		return readingElement;
	}
	
	public static ReadingElement extractReadingElement( List<XMLEvent> readingElementSingle)	{
		
		ReadingElement readingElement = new ReadingElement();
		
		return extractReadingElement(readingElementSingle, readingElement);
	}
	
}
