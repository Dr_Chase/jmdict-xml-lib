package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.sense;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.junit.BeforeClass;
import org.junit.Test;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.Sample1;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.XMLEventUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.sense.extractors.ExtractUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.jmdict.jmdict.Gloss;

public class GlossExtractorsTest {

	private static Sample1 sample;
	
	@BeforeClass
	public static void intialize() throws XMLStreamException	{
		
		sample = new Sample1();
	}
	
	@Test
	public void searchingAllGlossesInSample1()	{
		
		List<XMLEvent> xmlEvents = sample.getAllEventsAsList();
		
		xmlEvents = new ArrayList<>(xmlEvents);
		
		List<XMLEvent> xmlEventsAllSenses = XMLEventUtils.filterListOfXmlEvents(xmlEvents, JmDictElements.SENSE) ;
		
		List<Gloss> listOfAllGlosses = new ArrayList<>();
		
		while( !xmlEventsAllSenses.isEmpty() )	{
			
			List<XMLEvent> xmlEventsSingleSense 
					= XMLEventUtils.filterListOfXmlEventsForSingleElement(xmlEventsAllSenses, JmDictElements.SENSE);
			
			List<Gloss> allGlossInSingleSense =  ExtractUtils.getListOfGlosses(xmlEventsSingleSense);
			
			listOfAllGlosses.addAll(allGlossInSingleSense);
			
			xmlEventsAllSenses.removeAll(xmlEventsSingleSense);
		}
		
		Integer size = listOfAllGlosses.size();
		
		for(Gloss gloss : listOfAllGlosses) {
			
			System.out.println(gloss);
		}
		
		// 304 / 2 = 152
		assertEquals(Integer.valueOf(152), size);
		
		
	}
}
