package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.junit.Test;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.FilterUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.Sample1;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors.EntryExtractors;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors.K_EleExtractors;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.jmdict.jmdict.Entry;
import hu.unideb.jmdict.jmdict.KanjiElement;

public class EntryExtractorsTest {

	
	@Test
	public void extractingKanjiElements() throws XMLStreamException	{
		
		
		Sample1 sample = new Sample1();
		
		List<XMLEvent> xmlEvents = sample.getAllEventsAsList();
		
		List<XMLEvent> xmlEventsOfAllEntries = FilterUtils.filterFor(xmlEvents, JmDictElements.ENTRY);
		
		List<Entry> entries = EntryExtractors.extractEntries(xmlEventsOfAllEntries);
		
		Integer size = entries.size();
		
		System.out.println(size);
		
		for(Entry entry : entries) {
			
			System.out.println(entry);
		}
		
		assertEquals(Integer.valueOf(5), size );
	}
}
