package hu.unideb.dictionary.japanese.xml.jmdict;

import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang3.StringUtils;
//import org.codehaus.stax2.XMLInputFactory2;
import org.junit.Test;

//import com.ctc.wstx.stax.WstxInputFactory;

import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JMDictXMLEventsEntry;
import hu.unideb.dictionary.japanese.xml.jmdict.utils.ResourceHelpers;


public class ParseTest {

	@Test
	public void firstTest() {
		JMDictParser parser = new JMDictParser();

	}

	@Test
	public void printingOutStuff() throws XMLStreamException {

		InputStream jmDictIS;

		XMLEventReader xmlEventReader;

		XMLInputFactory xmlInputFactory;

		jmDictIS = ResourceHelpers.getResourceAsInputStream("JMDict");

		
		// So far I have not been able to change entity Expansion Limit (100000) which caused problem, so we turned it off
		// https://docs.oracle.com/javase/tutorial/jaxp/limits/using.html
		//System.setProperty("jdk.xml.entityExpansionLimit", "200000");
		
		// https://docs.oracle.com/javase/tutorial/jaxp/limits/limits.html   <--- this man is the best
		//System.setProperty("jdk.xml.maxGeneralEntitySizeLimit", "2000");
		
		
		
		xmlInputFactory = XMLInputFactory.newInstance(); //xmlInputFactory = WstxInputFactory.newInstance(); 
		
		//((WstxInputFactory) xmlInputFactory  ) .configureForLowMemUsage();
		
		
		
		xmlInputFactory.setProperty("javax.xml.stream.isReplacingEntityReferences", new Boolean(false));
		
		//assertTrue(xmlInputFactory.isPropertySupported("ENTITY_EXPANSION_LIMIT"));
		//xmlInputFactory.setProperty("ENTITY_EXPANSION_LIMIT", new Integer(200000) );
		//xmlInputFactory.setProperty(name, value);

		List<JMDictXMLEventsEntry> jmDictEntries = new ArrayList<JMDictXMLEventsEntry>();

		
		xmlEventReader = xmlInputFactory.createXMLEventReader(jmDictIS, "UTF-8");
		


		List<XMLEvent> jmDictEntryEvents = new ArrayList<XMLEvent>();

		boolean areWeAtTheStartOfEntry = false;
		boolean areWeAtTheEndOfEntry = false;
		
		boolean matchingWordFound = false;

		for (int index = 0; /* index < 250 */ xmlEventReader.hasNext() ; index++) {

			XMLEvent event = xmlEventReader.nextEvent();

			if (event.isStartElement()) {

				if (event.asStartElement().getName().toString().equals("entry")) {
					
					jmDictEntryEvents = new ArrayList<XMLEvent>();
					areWeAtTheStartOfEntry = true;
				}

			}

			if (areWeAtTheStartOfEntry == true && areWeAtTheEndOfEntry == false) {

				jmDictEntryEvents.add(event);
			}

			if (event.isEndElement()) {

				if (event.asEndElement().getName().toString().equals("entry")) {

					areWeAtTheEndOfEntry = true;
				}

			}
			
			
			if( event.isEndElement() && jmDictEntryEvents.size() >= 2 )	{
				
				XMLEvent previousEvent = jmDictEntryEvents.get( jmDictEntryEvents.size() - 2 );
				
				if( event.asEndElement().getName().toString().equals("gloss") && 
						previousEvent.isCharacters()  )	{
					
					String glossText = previousEvent.asCharacters().getData();
					
					if( StringUtils.contains(glossText,  "glass"  ) )	{
						matchingWordFound = true;
					}
					
				}
				
				
				
			}

			if (areWeAtTheStartOfEntry && areWeAtTheEndOfEntry) {

				if( matchingWordFound )	{
					jmDictEntries.add(new JMDictXMLEventsEntry(jmDictEntryEvents));
					
					printOut( jmDictEntries.get(jmDictEntries.size() - 1) );
					
					matchingWordFound = false;
				}
				areWeAtTheStartOfEntry = false;
				areWeAtTheEndOfEntry = false;

			}

		}
		
		xmlEventReader.close();
		
		//System.out.println("jmDictEntryEvents: " + jmDictEntryEvents.size());
		//System.out.println("jmDictEntries: " + jmDictEntries.size());

		//for (JMDictEntry entry : jmDictEntries) {

			//printOut(entry);
		//}

	}

	public void printOut(JMDictXMLEventsEntry entry) {
		List<XMLEvent> events = entry.getEntry();

		for( int index = 0; index < events.size(); index++ )	{
		//for (XMLEvent event : events) {
			
			XMLEvent event = events.get(index);
			
			if( event.getEventType() == XMLEvent.START_ELEMENT )	{
				System.out.print(event.asStartElement().getName().toString() );
				
				if(event.asStartElement().getName().toString() == "gloss")	{
					
					if( index + 1 < events.size() )	{
						
						Characters pcdata = events.get(index + 1).asCharacters();

						System.out.print( " : " + pcdata.getData() + " " + JMDictParser.getEventTypeString(events.get(index + 1).getEventType() ) );
						
					}
					
					/*
					// this does not work
					for(int innerIndex = index; events.get(innerIndex).getEventType() != XMLEvent.END_ELEMENT; innerIndex++ )	{
						
						XMLEvent innerEvent = events.get(innerIndex);
						
						if(innerEvent.isAttribute())	{
							
							System.out.println("Attribute found");
							
							System.out.println(innerEvent.asStartElement().getName());
						}
					} */
					
					Iterator iter = event.asStartElement().getAttributes();
					
					while(iter.hasNext())	{
						Object attribute = iter.next();  //Type in case of Xerces is com.sun.xml.internal.stream.events.AttributeImpl
						System.out.print(" attributes:  " + attribute ); // Type with woodstox or stax2 is org.codehaus.stax2.ri.evt.AttributeEventImpl
						System.out.println(attribute.getClass() + "  "); 
						com.sun.xml.internal.stream.events.AttributeImpl attributeCasted 
								= (com.sun.xml.internal.stream.events.AttributeImpl)  attribute;
						System.out.print("QName: " + "local part : " + attributeCasted.getName().getLocalPart() + 
									" namespace uri : " + attributeCasted.getName().getNamespaceURI()
								);
					}
					
					
					
					Attribute lang3Attr = event.asStartElement().getAttributeByName(new QName("lang"));
					//System.out.println("fasz");
					if(lang3Attr != null)	{
						
						System.out.println( lang3Attr.getValue() );
					}
					
					
				}
				
				System.out.println();
			}
			
			
			
			
		}
	}

}
