package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.junit.Test;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.FilterUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.Sample1;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors.R_EleExtractors;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.jmdict.jmdict.ReadingElement;

public class R_EleExtractorsTest {

	
	@Test
	public void testReadingElementExtractor() throws XMLStreamException	{
		
		Sample1 sample = new Sample1();
		
		List<XMLEvent> xmlEvents = sample.getAllEventsAsList();
		
		List<XMLEvent> xmlEventsOfAllReadingElements = FilterUtils.filterFor(xmlEvents, JmDictElements.READING_ELEMENT);
		
		List<ReadingElement> readingElements = R_EleExtractors.extractReadingElements(xmlEventsOfAllReadingElements);
		
		Integer size = readingElements.size();
		
		System.out.println(size);
		
		for(ReadingElement re : readingElements) {
			
			System.out.println(re);
		}
		
		assertEquals(Integer.valueOf(7), size );
		
	}
}
