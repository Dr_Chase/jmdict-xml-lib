package hu.unideb.dictionary.japanese.xml.jmdict.algorithm;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.junit.Test;

import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JMDictXMLEventsEntry;

public class XMLEventParserTest {

	
	@Test
	public void getAllXmlEvent() throws XMLStreamException	{
		
		List<JMDictXMLEventsEntry> list = XMLEventParser.getJMDictXMLEventsEntryFromFile();
		
		assertNotNull(list);
		assertTrue(list.size() > 0);
	}
}
