package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.helpers.GlossHelpers;

public class GlossHelpersTest {

	
	@Test
	public void testEngWithApostrophe()	{
		
		//Apostrophe is '
		String output = GlossHelpers.extractThreeLetterLangCodeAsString("xml:lang='eng'");
		
		assertEquals( "eng", output );
	}
	
	@Test
	public void testEngWithQuotationMark()	{
		
		//Apostrophe is '
		String output = GlossHelpers.extractThreeLetterLangCodeAsString("xml:lang=\"eng\"");
		
		assertEquals( "eng", output );
	}
	
	@Test
	public void testHunWithApostrophe()	{
		
		//Apostrophe is '
		String output = GlossHelpers.extractThreeLetterLangCodeAsString("xml:lang='hun'");
		
		assertEquals( "hun", output );
	}
	
	@Test
	public void testHunWithQuotationMark()	{
		
		//Apostrophe is '
		String output = GlossHelpers.extractThreeLetterLangCodeAsString("xml:lang=\"hun\"");
		
		assertEquals( "hun", output );
	}
}
