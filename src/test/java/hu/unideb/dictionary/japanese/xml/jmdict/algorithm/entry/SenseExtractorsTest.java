package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.junit.Test;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.FilterUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.Sample1;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors.SenseExtractors;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.jmdict.jmdict.Sense;

public class SenseExtractorsTest {

	@Test
	public void senseTest() throws XMLStreamException	{
		
		Sample1 sample = new Sample1();
		
		List<XMLEvent> xmlEvents = sample.getAllEventsAsList();
		
		List<XMLEvent> xmlEventsOfAllSenseElements = FilterUtils.filterFor(xmlEvents, JmDictElements.SENSE);
		
		List<Sense> senseElements = SenseExtractors.extractSensesForEntry(xmlEventsOfAllSenseElements);
		
		Integer size = senseElements.size();
		
		System.out.println(size);
		
		for(Sense re : senseElements) {
			
			System.out.println(re);
		}
		
		assertEquals(Integer.valueOf(42), size );
	}
}
