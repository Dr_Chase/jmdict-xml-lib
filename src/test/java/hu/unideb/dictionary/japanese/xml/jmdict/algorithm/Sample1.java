package hu.unideb.dictionary.japanese.xml.jmdict.algorithm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.junit.BeforeClass;

import hu.unideb.dictionary.japanese.xml.jmdict.utils.XMLDocumentHandler;

public class Sample1 {

	
	private volatile  XMLEventReader eventReader;
	
	private volatile  List<XMLEvent> allEventsAsList = new ArrayList<>();

	public Sample1() throws XMLStreamException	{
		
		XMLEventReader eventReader = XMLDocumentHandler.getEventReaderFor("JMDict-sample1.xml");
		
		while(eventReader.hasNext())	{
			
			XMLEvent event = eventReader.nextEvent();
			
			allEventsAsList.add(event);
		}
	}

	public  XMLEventReader getEventReader() {
		return eventReader;
	}

	public  List<XMLEvent> getAllEventsAsList() {
		return new ArrayList<>(allEventsAsList);
	}
	
	
}
