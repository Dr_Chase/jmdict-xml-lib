package hu.unideb.dictionary.japanese.xml.jmdict.algorithm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.junit.BeforeClass;
import org.junit.Test;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.sense.extractors.ExtractUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.dictionary.japanese.xml.jmdict.utils.XMLDocumentHandler;
import hu.unideb.jmdict.jmdict.Gloss;
import junit.framework.Assert;

public class XMLEventUtilsTest {
	
	private static XMLEventReader eventReader;
	
	private static List<XMLEvent> allEventsAsList = new ArrayList<>();

	@BeforeClass
	public static void initialize() throws XMLStreamException	{
		
		XMLEventReader eventReader = XMLDocumentHandler.getEventReaderFor("JMDict-sample1.xml");
		
		while(eventReader.hasNext())	{
			
			XMLEvent event = eventReader.nextEvent();
			
			allEventsAsList.add(event);
		}
	}

	
	@Test
	public void printOutStuff()	{
		
		List<XMLEvent> entry = FilterUtils.filterForSingleElement(allEventsAsList, JmDictElements.ENTRY);
		
		List<XMLEvent> sense = FilterUtils.filterForSingleElement(entry, JmDictElements.SENSE);
		
		List<Gloss> glosses = ExtractUtils.getListOfGlosses(sense);
		
		for(Gloss gloss : glosses)	{
			
			System.out.println(gloss);
		}
	}
	
	@Test
	public void doesItEnd()	{
		
		List<XMLEvent> entry = FilterUtils.filterForSingleElement(allEventsAsList, JmDictElements.ENTRY);
		
		List<XMLEvent> sense = FilterUtils.filterForSingleElement(entry, JmDictElements.SENSE);
		
		Assert.assertTrue(true);
	}
}
