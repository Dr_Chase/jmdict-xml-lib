package hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.junit.Test;

import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.FilterUtils;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.Sample1;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors.K_EleExtractors;
import hu.unideb.dictionary.japanese.xml.jmdict.algorithm.entry.extractors.R_EleExtractors;
import hu.unideb.dictionary.japanese.xml.jmdict.model.xml.JmDictElements;
import hu.unideb.jmdict.jmdict.KanjiElement;
import hu.unideb.jmdict.jmdict.ReadingElement;

public class K_EleExtractorsTest {

	@Test
	public void extractingKanjiElements() throws XMLStreamException	{
		
		
		Sample1 sample = new Sample1();
		
		List<XMLEvent> xmlEvents = sample.getAllEventsAsList();
		
		List<XMLEvent> xmlEventsOfAllKanjiElements = FilterUtils.filterFor(xmlEvents, JmDictElements.KANJI_ELEMENT);
		
		List<KanjiElement> kanjiElements = K_EleExtractors.extractKanjiElements(xmlEventsOfAllKanjiElements);
		
		Integer size = kanjiElements.size();
		
		System.out.println(size);
		
		for(KanjiElement re : kanjiElements) {
			
			System.out.println(re);
		}
		
		assertEquals(Integer.valueOf(7), size );
	}
}
